#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
import config
import jwt
import requests
import time
import re
from bs4 import BeautifulSoup
from flask import Flask, jsonify, request

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
sys.path.append(BASE_DIR)

activate_this = os.path.join(BASE_DIR, 'venv3/bin/activate_this.py')
exec(open(activate_this).read())


# clickatlife - web scraping - specify the url
clickatlife = 'http://www.clickatlife.gr/atzenta'

# query the website and return the html to the variable 'page'
page = requests.get(clickatlife)
soup = BeautifulSoup(page.content, from_encoding='utf-8')

hrefs = []
counter = 0
events = soup.find("div", id="panelContainer")
# print(events)
# titles = events.find_all('a', href=True)
for title in events.find_all('a'):
    hrefs.append(title['href'])
    # print(hrefs[counter])
    counter = counter + 1

# Διόρθωση της λίστας, φιλτράρισμα των αποτελεσμάτων
links = []
counter = 0
for i in range(0, len(hrefs)):
    if ("/atzenta?c=" in hrefs[i]):
        # print(hrefs[i])
        continue
        # hrefs.pop(i)
    elif ("javascript:void(0)" in hrefs[i]):
        # print(hrefs[i])
        continue
        # hrefs.pop(i)
    # για να γλιτώσουμε τα διπλότυπα
    elif (i != len(hrefs) - 1 and hrefs[i] == hrefs[i + 1]):
        # print(hrefs[i])
        continue
        # hrefs.pop(i + 1)
    links.append(hrefs[i])
    counter = counter + 1

print("Πλήθος link : ", len(links))

tts = []
counter = 0
for title in events.find_all('a'):
    # print("title text : ", title.text.decode('utf-8'))
    if (title.text is None or title.text == u'  ' or title.text == u''):
        continue
    tts.append(title.text)
    # print(tts[counter])
    counter = counter + 1

tts.pop()
print("Πλήθος titles : ", len(tts))

if len(links) == len(tts):
    msg_ready = "READY FOR INSERTION"
else:
    msg_ready = "NEEDS MORE FILTERING. INSERTION ISNT COMPLETED"

print(msg_ready)

# clickatlife - web scraping - specify the url
clickatlife_general = 'http://www.clickatlife.gr'

# πραγματοποιούμε ένα loop για κάθε link από event που διαθέτουμε
counter = 0
names = tts
addresses = []
fees = []
times = []
lats = []
lngs = []
descriptions = []
imgs = []

for j in range(0, len(links)):
    # δινουμε λιγο χρονο για να μην δημιουργήσει πρόβλημα στη σελίδα
    time.sleep(2)
    page = requests.get(clickatlife_general + links[j])

    soup = BeautifulSoup(page.content, from_encoding='utf-8')

    info = soup.find(
        "div", class_="box")
    media = soup.find(
        "div", id="leftPHArea_PageViewPhotoStory")
    counter = 0
    descriptions.append("")
    addresses.append("")
    fees.append("")
    times.append("")
    lats.append("")
    lngs.append("")
    descriptions.append("")
    imgs.append("")

    for p in info.find_all('input'):
        try:

            # print(p['value'])
            if (counter == 2):
                lats[j] = p['value']
            if (counter == 3):
                lngs[j] = p['value']
            if (counter == 6):
                addresses[j] = p['value']
            counter += 1
        except KeyError:
            counter += 1
            continue

    info = soup.find(
        "div", class_="event-info-container")

    for p in media.find_all('img'):
        try:
            imgs[j] = "www.clickatlife.gr" + p['src']
            print(imgs[j])
        except KeyError:
            continue

    for p in info.find_all('p'):

        try:
                # print(p.text)
            text = p.text
            if u"Ώρα" in p.text:
                times[j] = re.search(r'\d{2}:\d{2}', text).group()
                # print("time : ", times[j])
            elif u"ευρώ" in text:
                fees[j] = text
                # print("fee : ", fees[j])
            else:
                descriptions[j] += " " + p.text

        except KeyError:

            continue
    print("==========================================//==================//=============================================")
    print("name: ", names[j], "address: ", addresses[j], "lat: ", lats[j], "lng: ",
          lngs[j], "fee: ", fees[j], "time: ", times[j], "description", descriptions[j])

    if names[j] is None or names[j] == "":
        msg_ready = "no name in " + str(j) + "th element"
        print("no name")
        continue
    if addresses[j] is None or addresses[j] == "":
        msg_ready = "no address in " + str(j) + "th element"
        print("no address")
        continue
    if lats[j] is None or lats[j] == "":
        msg_ready = "no lat in " + str(j) + "th element"
        print("no lat")
        continue
    if lngs[j] is None or lngs[j] == "":
        msg_ready = "no lng in " + str(j) + "th element"
        print("no lng")
        continue
    if times[j] is None or times[j] == "":
        msg_ready = "no time in " + str(j) + "th element"
        print("no time")
        continue
