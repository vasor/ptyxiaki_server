#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
import config
import jwt
import urllib2
import requests
import time
import re
from bs4 import BeautifulSoup
from flask import Flask, jsonify, request
app = Flask(__name__)

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
sys.path.append(BASE_DIR)

activate_this = os.path.join(BASE_DIR, 'venv/bin/activate_this.py')
execfile(activate_this, dict(__file__=activate_this))


# clickatlife - web scraping - specify the url
clickatlife = 'http://www.clickatlife.gr/atzenta'

# query the website and return the html to the variable 'page'
page = requests.get(clickatlife)
soup = BeautifulSoup(page.content, from_encoding='utf-8')

# parse the html using beautiful soap and store in variable `soup`
# soup = BeautifulSoup(page, 'html.parser')
#  event = soup.findAll("h3", class_="minus")
hrefs = []
counter = 0
events = soup.find("div", id="panelContainer")
print(events)
# titles = events.find_all('a', href=True)
for title in events.find_all('a'):
    hrefs.append(title['href'])
    print(hrefs[counter])
    counter = counter + 1

# Διόρθωση της λίστας, φιλτράρισμα των αποτελεσμάτων
links = []
counter = 0
for i in range(0, len(hrefs)):
    if ("/atzenta?c=" in hrefs[i]):
        print(hrefs[i])
        continue
        # hrefs.pop(i)
    elif ("javascript:void(0)" in hrefs[i]):
        print(hrefs[i])
        continue
        # hrefs.pop(i)
    # για να γλιτώσουμε τα διπλότυπα
    elif (i != len(hrefs) - 1 and hrefs[i] == hrefs[i + 1]):
        print(hrefs[i])
        continue
        # hrefs.pop(i + 1)
    links.append(hrefs[i])
    counter = counter + 1


print("Πλήθος link : ", len(links))

tts = []
counter = 0
for title in events.find_all('a'):
    # print("title text : ", title.text.decode('utf-8'))
    if (title.text is None or title.text == u'  ' or title.text == u''):
        continue
    tts.append(title.text)
    print(tts[counter])
    counter = counter + 1

tts.pop()
print("Πλήθος titles : ", len(tts))

if len(links) == len(tts):
    msg_ready = "READY FOR INSERTION"
else:
    msg_ready = "NEEDS MORE FILTERING. INSERTION ISNT COMPLETED"

print(msg_ready)


# opening all other links to get more data about each event using for loop
# page2 = urllib2.urlopen('http://www.clickatlife.gr' +
#                         listevents[3]['href'])
# soup2 = BeautifulSoup(page2.read().decode('utf-8', 'ignore'), 'html.parser')

# address = soup2.find(
#     "input", class_="field-plus disabled right", value=True)['value']
# print("Διεύθυνση: ", address)


# clickatlife - web scraping - specify the url
clickatlife_general = 'http://www.clickatlife.gr'

# query the website and return the html to the variable 'page'
counter = 0
addresses = []

page = requests.get(clickatlife_general + links[0])

soup = BeautifulSoup(page.content, from_encoding='utf-8')

info = soup.find(
    "div", class_="box")

counter = 1
for p in info.find_all('input'):
    try:

        print(p['value'])
        if (counter == 3):
            lat = p['value']
        if (counter == 4):
            lng = p['value']
        counter += 1
    except KeyError:
        counter += 1
        continue

info = soup.find(
    "div", class_="event-info-container")

description = ""

for p in info.find_all('p'):

    try:
        print(p.text)
        text = p.text
        if u"Ώρα" in p.text:
            time = map(int, re.findall(r'\d+', text))
            print("time : ", time)
        elif u"Είσοδος" in text:
            fee = map(int, re.findall(r'\d+', text))
            print("fee : ", fee)
        else:
            description += " " + p.text

    except KeyError:

        continue

print("all info: ", lat, lng, fee, time, description.encode('greek'))
# for i in range(0, len(links)):

#     page = requests.get(clickatlife_general + links[i])

#     soup = BeautifulSoup(page.content, from_encoding='utf-8')
#     print(soup.original_encoding)

#     info = soup.find(
#         "div", class_="input-container cf", value=True)

#     print(info)
#     addresses.append(info)
#     print("type: ", type(addresses[i]))
#     time.sleep(1.5)
