#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
import config
import jwt
import requests
import re
import time
from functools import wraps
from bs4 import BeautifulSoup
from flask import Flask, jsonify, request, g
from pymongo import MongoClient
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime
from datetime import timedelta
from flask_cors import cross_origin
from jwt import DecodeError, ExpiredSignature

app = Flask(__name__)

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
sys.path.append(BASE_DIR)

activate_this = os.path.join(BASE_DIR, 'venv3/bin/activate_this.py')
exec(open(activate_this).read())

print("ready")

# CONNECTS WITH MONGODB FROM THE CLOUD SERVICE MLAB


def get_mongo_client():
    return MongoClient('mongodb://admin:admin91114@ds157839.mlab.com:57839/eventsapp')


client = get_mongo_client()
userscol = client.eventsapp.users
eventscol = client.eventsapp.events


# TODO : Check as well if user has been deleted.
def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        token = request.headers.get('Authorization')
        if not token:
            response = jsonify(message='Missing authorization header')
            response.status_code = 401
            return response
        try:
            payload = jwt.decode(token.split()[1], config.JWT_SECRET_KEY)
        except DecodeError:
            response = jsonify(message='Token is invalid')
            response.status_code = 401
            return response
        except ExpiredSignature:
            response = jsonify(message='Token has expired')
            response.status_code = 401
            return response

        g.user_id = payload['sub']

        return f(*args, **kwargs)

    return decorated_function


def create_token(user_id):
    payload = {
        'sub': user_id,
        'iat': datetime.utcnow(),
        'exp': datetime.utcnow() + timedelta(days=14)
    }
    token = jwt.encode(payload, config.JWT_SECRET_KEY)
    return token.decode('unicode_escape')

# STARTS FROM HERE - TODO : DEAMON/CRON EVERY DAY
# clickatlife - web scraping - specify the url
clickatlife = 'http://www.clickatlife.gr/atzenta'

# query the website and return the html to the variable 'page'
page = requests.get(clickatlife)
soup = BeautifulSoup(page.content, from_encoding='utf-8')

hrefs = []
counter = 0
events = soup.find("div", id="panelContainer")
# print(events)
# titles = events.find_all('a', href=True)
for title in events.find_all('a'):
    hrefs.append(title['href'])
    # print(hrefs[counter])
    counter = counter + 1

# Διόρθωση της λίστας, φιλτράρισμα των αποτελεσμάτων
links = []
counter = 0
for i in range(0, len(hrefs)):
    if ("/atzenta?c=" in hrefs[i]):
        # print(hrefs[i])
        continue
        # hrefs.pop(i)
    elif ("javascript:void(0)" in hrefs[i]):
        # print(hrefs[i])
        continue
        # hrefs.pop(i)
    # για να γλιτώσουμε τα διπλότυπα
    elif (i != len(hrefs) - 1 and hrefs[i] == hrefs[i + 1]):
        # print(hrefs[i])
        continue
        # hrefs.pop(i + 1)
    links.append(hrefs[i])
    counter = counter + 1

print("Πλήθος link : ", len(links))

tts = []
counter = 0
for title in events.find_all('a'):
    # print("title text : ", title.text.decode('utf-8'))
    if (title.text is None or title.text == u'  ' or title.text == u''):
        continue
    tts.append(title.text)
    # print(tts[counter])
    counter = counter + 1

tts.pop()
print("Πλήθος titles : ", len(tts))

if len(links) == len(tts):
    msg_ready = "READY FOR INSERTION"
else:
    msg_ready = "NEEDS MORE FILTERING. INSERTION ISNT COMPLETED"

print(msg_ready)

# clickatlife - web scraping - specify the url
clickatlife_general = 'http://www.clickatlife.gr'

# πραγματοποιούμε ένα loop για κάθε link από event που διαθέτουμε
counter = 0
names = tts
addresses = []
fees = []
times = []
lats = []
lngs = []
descriptions = []
for j in range(0, len(links)):
    # δινουμε λιγο χρονο για να μην δημιουργήσει πρόβλημα στη σελίδα
    time.sleep(2)
    page = requests.get(clickatlife_general + links[j])

    soup = BeautifulSoup(page.content, from_encoding='utf-8')

    info = soup.find(
        "div", class_="box")

    counter = 0
    descriptions.append("")
    addresses.append("")
    fees.append("")
    times.append("")
    lats.append("")
    lngs.append("")
    descriptions.append("")
    for p in info.find_all('input'):
        try:

            # print(p['value'])
            if (counter == 2):
                lats[j] = p['value']
            if (counter == 3):
                lngs[j] = p['value']
            if (counter == 6):
                addresses[j] = p['value']
            counter += 1
        except KeyError:
            counter += 1
            continue

    info = soup.find(
        "div", class_="event-info-container")

    for p in info.find_all('p'):

        try:
            # print(p.text)
            text = p.text
            if u"Ώρα" in p.text:
                times[j] = re.search(r'\d{2}:\d{2}', text).group()
                # print("time : ", times[j])
            elif u"ευρώ" in text:
                fees[j] = text
                # print("fee : ", fees[j])
            else:
                descriptions[j] += " " + p.text

        except KeyError:

            continue
    print("==========================================//==================//=============================================")
    print("name: ", names[j], "address: ", addresses[j], "lat: ", lats[j], "lng: ",
          lngs[j], "fee: ", fees[j], "time: ", times[j], "description", descriptions[j])

    if names[j] is None or names[j] == "":
        msg_ready = "no name in " + str(j) + "th element"
        print("no name")
        continue
    if addresses[j] is None or addresses[j] == "":
        msg_ready = "no address in " + str(j) + "th element"
        print("no address")
        continue
    if lats[j] is None or lats[j] == "":
        msg_ready = "no lat in " + str(j) + "th element"
        print("no lat")
        continue
    if lngs[j] is None or lngs[j] == "":
        msg_ready = "no lng in " + str(j) + "th element"
        print("no lng")
        continue
    if times[j] is None or times[j] == "":
        msg_ready = "no time in " + str(j) + "th element"
        print("no time")
        continue


# ========= ROUTES ==========


@app.route('/info')
def show_info():

    return jsonify("all info: ", lats, lngs, fees, times, descriptions, names, addresses)


@app.route('/list', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
@login_required
def get_modules_list():
    # TODO : Maybe get device_id from DB ?
    device_id = request.json.get('device_id')
    data = [event for event in eventscol.find(
        {}, {"_id": 0})]
    return jsonify(data)


# @app.route('/insert')
# def insert():
#     if msg_ready == "READY FOR INSERTION":
#         eventscol.delete_many({})
#         for i in range(0, len(links)):
#             eventscol.insert({'url': links[i], 'title': tts[i], 'address': addresses[i], "coordinates": {
#                              "lat": lats[i], "lng": lngs[i]}, "price": fees[i], "time": times[i], "description": descriptions[i]})
#         msg_insertion = "insertion completed"
#     else:
#         msg_insertion = msg_ready
#     return jsonify({"message": msg_insertion})


@app.route('/login', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def login():
    email = request.json.get('email')
    password = request.json.get('password')

    found = userscol.find_one({'email': email})

    if not found:
        response = jsonify(message='Not existent email')
        response.status_code = 404
        return response

    if check_password_hash(found['password'], password):
        token = create_token(str(found['_id']))
        return jsonify(token=token)
    else:
        response = jsonify(message='Wrong Email or Password')
        response.status_code = 401
        return response


@app.route('/signup', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def signup():
    name = request.json.get('name')
    surname = request.json.get('surname')
    username = request.json.get('username')
    email = request.json.get('email')
    password = request.json.get('password')

    if not name:
        response = jsonify(message='No name inserted')
        response.status_code = 401
        return response

    if not surname:
        response = jsonify(message='No surnname inserted')
        response.status_code = 401
        return response

    if not username:
        response = jsonify(message='No username inserted')
        response.status_code = 401
        return response

    if not email:
        response = jsonify(message='No email inserted')
        response.status_code = 401
        return response

    if not password:
        response = jsonify(message='No password inserted')
        response.status_code = 401
        return response

    if userscol.find_one({'email': email}):
        response = jsonify(message='Email already in use')
        response.status_code = 401
        return response

    password = generate_password_hash(password)
    user_id = userscol.insert({'name': name, 'surname': surname, 'username': username,
                               'email': email, 'password': password, 'registered_at': datetime.now()})
    token = create_token(str(user_id))
    return jsonify(token=token)


if __name__ == '__main__':
    app.run(
        host=config.HOST,
        port=config.PORT,
        debug=config.DEBUG,
        threaded=config.THREADED
    )
