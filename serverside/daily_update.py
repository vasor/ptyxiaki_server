#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
# import urllib

# from urllib.request import urlretrieve
import base64
import config
import jwt
import requests
import time
import re
import schedule
from bs4 import BeautifulSoup
from pymongo import MongoClient
from flask import Flask, jsonify, request
from database_helper import get_mongo_client

client = get_mongo_client()
eventscol = client.eventsapp.events

# schedule.every().day.at("21:05").do(update_info)

# while 1:
#     schedule.run_pending()
#     time.sleep(1)


def update_info():
    # clickatlife - web scraping - specify the url
    clickatlife = 'http://www.clickatlife.gr/atzenta'

    # query the website and return the html to the variable 'page'
    page = requests.get(clickatlife)
    soup = BeautifulSoup(page.content, from_encoding='utf-8')

    hrefs = []
    counter = 0
    events = soup.find("div", id="panelContainer")
    # print(events)
    # titles = events.find_all('a', href=True)
    for title in events.find_all('a'):
        hrefs.append(title['href'])
        # print(hrefs[counter])
        counter = counter + 1

    # Διόρθωση της λίστας, φιλτράρισμα των αποτελεσμάτων
    links = []
    counter = 0
    for i in range(0, len(hrefs)):
        if ("/atzenta?c=" in hrefs[i]):
            # print(hrefs[i])
            continue
            # hrefs.pop(i)
        elif ("javascript:void(0)" in hrefs[i]):
            # print(hrefs[i])
            continue
            # hrefs.pop(i)
        # για να γλιτώσουμε τα διπλότυπα
        elif (i != len(hrefs) - 1 and hrefs[i] == hrefs[i + 1]):
            # print(hrefs[i])
            continue
            # hrefs.pop(i + 1)
        links.append(hrefs[i])
        counter = counter + 1

    print("Πλήθος link : ", len(links))

    tts = []
    counter = 0
    for title in events.find_all('a'):
        # print("title text : ", title.text.decode('utf-8'))
        if (title.text is None or title.text == u'  ' or title.text == u''):
            continue
        tts.append(title.text)
        # print(tts[counter])
        counter = counter + 1

    tts.pop()
    print("Πλήθος titles : ", len(tts))

    if len(links) == len(tts):
        msg_ready = "READY FOR INSERTION"
    else:
        msg_ready = "NEEDS MORE FILTERING. INSERTION ISNT COMPLETED"

    print(msg_ready)

    # clickatlife - web scraping - specify the url
    clickatlife_general = 'http://www.clickatlife.gr'

    # πραγματοποιούμε ένα loop για κάθε link από event που διαθέτουμε
    counter = 0
    names = tts
    addresses = []
    fees = []
    times = []
    lats = []
    lngs = []
    imgs = []
    descriptions = []
    ok = []  # για να ελεγχουμε αν ειναι επαρκη τα στοιχεια. πχ αν δινεται τοποθεσια, ωρα κτλ
    for j in range(0, len(links)):
        # δινουμε λιγο χρονο για να μην δημιουργήσει πρόβλημα στη σελίδα
        time.sleep(2)
        page = requests.get(clickatlife_general + links[j])

        soup = BeautifulSoup(page.content, from_encoding='utf-8')

        info = soup.find(
            "div", class_="box")

        counter = 0
        descriptions.append("")
        addresses.append("")
        fees.append("")
        times.append("")
        lats.append("")
        lngs.append("")
        descriptions.append("")
        ok.append("")
        imgs.append("")
        for p in info.find_all('input'):
            try:

                # print(p['value'])
                if (counter == 2):
                    lats[j] = p['value']
                if (counter == 3):
                    lngs[j] = p['value']
                if (counter == 6):
                    addresses[j] = p['value']
                counter += 1
                # KAMIA FORA TA VAZEI ANAPODA. KANONIKA TA LNGS 9A PREPEI NA EINAI GYRW STA 37, ENW TA
                # LATS EINAI GYRW STA 23
                if lats[j] < lngs[j]:
                    temp = lats[j]
                    lats[j] = lngs[j]
                    lngs[j] = temp

            except KeyError:
                counter += 1
                continue

        info = soup.find(
            "div", class_="event-info-container")
        media = soup.find(
            "div", id="leftPHArea_PageViewPhotoStory")
        for p in media.find_all('img'):
            try:
                imgs[j] = "www.clickatlife.gr" + p['src']
                print(imgs[j])
            except KeyError:
                continue

        for p in info.find_all('p'):

            try:
                # if re.search(r'\d{2}:\d{2}', p.text) is not None:
                if u"Ώρα" in p.text or u"ώρα" in p.text or u"Έναρξη" in p.text or u"έναρξη" in p.text or u"Πόρτες" in p.text or u"πόρτες" in p.text or u"ώρες" in p.text or u"Ώρες" in p.text:
                    times[j] = re.findall(r'\d{2}:\d{2}', p.text)
                    # print("time : ", times[j])
                elif u"ευρώ" in p.text:
                    fees[j] = p.text
                    # print("fee : ", fees[j])
                else:
                    descriptions[j] += " " + p.text

            except KeyError:

                continue
        print("==========================================//==================//=============================================")
        print("name: ", names[j], "address: ", addresses[j], "lat: ", lats[j], "lng: ",
              lngs[j], "fee: ", fees[j], "time: ", times[j], "description", descriptions[j], "img", imgs[j])
        ok[j] = "ok"
        if names[j] is None or names[j] == "" or not names[j]:
            msg_ready = "no name in " + str(j) + "th element"
            print("no name")
            ok[j] = "no"
            continue
        if addresses[j] is None or addresses[j] == "" or not addresses[j]:
            msg_ready = "no address in " + str(j) + "th element"
            print("no address")
            ok[j] = "no"
            continue
        if lats[j] is None or lats[j] == "" or not lats[j]:
            msg_ready = "no lat in " + str(j) + "th element"
            print("no lat")
            ok[j] = "no"
            continue
        if lngs[j] is None or lngs[j] == "" or not lngs[j]:
            msg_ready = "no lng in " + str(j) + "th element"
            print("no lng")
            ok[j] = "no"
            continue
        if times[j] is None or times[j] == "" or not times[j]:
            msg_ready = "no time in " + str(j) + "th element"
            print("no time")
            ok[j] = "no"
            continue
        if imgs[j] is None or imgs[j] == "" or not imgs[j]:
            msg_ready = "no img in " + str(j) + "th element"
            print("no img")
            imgs[
                j] = "visitfairfieldbay.com/wp-content/uploads/2016/12/noPicture.png"
            continue

    eventscol.delete_many({})

    for i in range(0, len(links)):
        if ok[i] == "ok":
            # urlretrieve("http://" + str(imgs[j]))
            imgs[i] = "http://" + str(imgs[i])
            # print("times: ", times[i])

            # 'img': base64.b64encode(requests.get(imgs[i]).content)
            eventscol.insert({'img': imgs[i], 'url': links[i], 'title': tts[i],
                              'address': addresses[i], "coordinates": {"lat": lats[i], "lng": lngs[i]},
                              "price": fees[i], "starting_time": times[i][0],
                              "description": descriptions[i], "expected_duration": "02:00", "created_by": "clickatlife.gr"})
            msg_insertion = "insertion completed"

    

    # remove duplicates
    cursor = eventscol.aggregate(
        [
            {"$group": {"_id": "$title", "unique_ids": {
                "$addToSet": "$_id"}, "count": {"$sum": 1}}},
            {"$match": {"count": {"$gte": 2}}}
        ]
    )

    response = []
    for doc in cursor:
        del doc["unique_ids"][0]
        for id in doc["unique_ids"]:
            response.append(id)

    eventscol.remove({"_id": {"$in": response}}
