#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import config
from pymongo import MongoClient


def get_mongo_client():
    return MongoClient('mongodb://admin:admin91114@ds157839.mlab.com:57839/eventsapp')


client = get_mongo_client()
userscol = client.eventsapp.users
eventscol = client.eventsapp.events
