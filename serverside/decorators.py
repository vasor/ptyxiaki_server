#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import jwt
from functools import wraps
from flask import request, jsonify, g
from jwt import DecodeError, ExpiredSignature

import config


# TODO : Check as well if user has been deleted.
def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        token = request.headers.get('Authorization')
        if not token:
            response = jsonify(message='Missing authorization header')
            response.status_code = 401
            return response
        try:
            payload = jwt.decode(token.split()[1], config.JWT_SECRET_KEY)
        except DecodeError:
            response = jsonify(message='Token is invalid')
            response.status_code = 401
            return response
        except ExpiredSignature:
            response = jsonify(message='Token has expired')
            response.status_code = 401
            return response

        g.user_id = payload['sub']

        return f(*args, **kwargs)

    return decorated_function
