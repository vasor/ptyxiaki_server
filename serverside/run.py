#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
import base64
import config
import jwt
import requests
import re
import time
import schedule
from bson.objectid import ObjectId
from functools import wraps
from bs4 import BeautifulSoup
from flask import Flask, jsonify, request, g
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime
from datetime import timedelta
from flask_cors import cross_origin
from jwt import DecodeError, ExpiredSignature
from database_helper import get_mongo_client
from decorators import login_required
from daily_update import update_info
from threading import Timer
from datetime import timedelta
from flask_cors import cross_origin
from werkzeug.security import generate_password_hash, check_password_hash


# schedule updater to do update_info() every 00:10
x = datetime.today()
y = x.replace(day=x.day + 1, hour=00, minute=30, second=0, microsecond=0)
delta_t = y - x

secs = delta_t.seconds + 1


t = Timer(secs, update_info())
t.start()

app = Flask(__name__)

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
sys.path.append(BASE_DIR)

activate_this = os.path.join(BASE_DIR, 'venv3/bin/activate_this.py')
exec(open(activate_this).read())

print("ready")

# CONNECTS WITH MONGODB FROM THE CLOUD SERVICE MLAB

client = get_mongo_client()
userscol = client.eventsapp.users
eventscol = client.eventsapp.events
othereventscol = client.eventsapp.otherevents


# UPDATE INFO
# update = input("UPDATE INFO?")
# if update == "Y":
#     update_info()


def create_token(user_id):
    payload = {
        'sub': user_id,
        'iat': datetime.utcnow(),
        'exp': datetime.utcnow() + timedelta(days=14)
    }
    token = jwt.encode(payload, config.JWT_SECRET_KEY)
    return token.decode('unicode_escape')


# ========= ROUTES ==========


@app.route('/list')
@cross_origin(headers=['Content-Type', 'Authorization'])
def get_events_list():
    # edw einai sth morfh dd/mm/yy ara evala to 20 prin to yy gia na einai 20yy
    d = datetime.today().strftime("%d/%m/20%y")
    # print(d)
    # select events added by users
    dt = [event for event in othereventscol.find(
        {'date': d}, {"_id": 0})]
    # print(dt)
    data = [event for event in eventscol.find(
        {}, {"_id": 0})]
    counter = 0
    for i in range(0, len(dt)):
        data.append(dt[counter])
        counter += 1
    # print(data)
    t = str(datetime.time(datetime.now()))
    # parse first 5 characters
    t = t[0:5]
    fdata = []
    for j in range(0, len(data)):
        if (data[j]['starting_time'] <= t):
            # print(t)
            # print("minutes addition: ", data[j]['starting_time'][3:5],
            #       data[j]['expected_duration'][3:5])
            minutes = int(data[j]['starting_time'].split(':')[1]) + \
                int(data[j]['expected_duration'].split(':')[1])
            # print("hours addition: ",  data[j]['starting_time'][
            #       0:2], data[j]['expected_duration'][0:2])
            hours = int(data[j]['starting_time'].split(':')[0]) + \
                int(data[j]['expected_duration'].split(':')[0])

            if (minutes >= 60):
                minutes = minutes % 60
                hours += 1

            shours = str(hours)
            if (minutes < 10):
                sminutes = "0" + str(minutes)
            else:
                sminutes = str(minutes)
            # print(shours, ":", sminutes)
            stime = shours + ":" + sminutes
            if (t <= stime):
                fdata.append(data[j])
                # print(data[j])

    return jsonify(fdata)


@app.route('/login', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def login():
    username = request.json.get('username')
    password = request.json.get('password')

    found = userscol.find_one({'username': username})

    if not found:
        response = jsonify(message='Not existent username')
        response.status_code = 404
        return response

    if check_password_hash(found['password'], password):
        token = create_token(str(found['_id']))
        return jsonify(token=token)
    else:
        response = jsonify(message='Wrong username or Password')
        response.status_code = 401
        return response


@app.route('/all', methods=['GET'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def getall():
    d = datetime.today().strftime("%d/%m/20%y")
    events = [event for event in othereventscol.find(
        {'date': d}, {'_id': 1, 'img': 1, 'title': 1, 'description': 1, 'address': 1, 'price': 1, 'date': 1, 'starting_time': 1, 'expected_duration': 1, 'coordinates': 1, 'created_by': 1})]

    for i in range(0, len(events)):
        events[i]['_id'] = str(events[i]['_id'])

    events2 = [event for event in eventscol.find(
        {}, {'_id': 1, 'title': 1, 'img': 1, 'description': 1, 'address': 1, 'price': 1, 'date': 1, 'starting_time': 1, 'expected_duration': 1, 'coordinates': 1, 'created_by': 1})]
    # print(events)
    for i in range(0, len(events2)):
        events2[i]['_id'] = str(events2[i]['_id'])
    for i in range(0, len(events2)):
        events.append(events2[i])

    return jsonify(events)


@app.route('/info', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def getinfo():
    user_id = request.get_json()['user_id']
    # print(user_id)
    found = userscol.find_one({'_id': ObjectId(user_id)}, {
                              '_id': 0, 'password': 0})
    # print(found)

    events = [event for event in othereventscol.find(
        {'created_by': found['username']}, {'_id': 1, 'title': 1, 'description': 1, 'address': 1, 'price': 1, 'date': 1, 'starting_time': 1, 'expected_duration': 1, 'coordinates': 1})]

    for i in range(0, len(events)):
        events[i]['_id'] = str(events[i]['_id'])
    # print(events)
    response = jsonify(username=found['username'], name=found['name'], surname=found[
                       'surname'], email=found['email'], registered_at=found['registered_at'], events=events)
    return response


@app.route('/update_event', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def update_event():
    data = request.get_json()
    othereventscol.update_one({'_id': ObjectId(data['_id'])}, {'$set': data})
    return jsonify(message='User has been updated')


@app.route('/delete_event', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def delete_event():
    data = request.get_json()
    othereventscol.remove({'_id': ObjectId(data['event_id'])})
    return jsonify(message='User has been deleted')


@app.route('/insert', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def insert():
    # print(request.get_json()['title'])
    data = request.get_json()
    title = data['title']
    description = data['description']
    address = data['address']
    price = data['price']
    date = data['date']
    user_id = data['user_id']
    date2 = date.split('-')
    date = date2[2] + "/" + date2[1] + "/" + date2[0]
    starting_time = data['starting_time']
    expected_duration = data['expected_duration']
    position = data['position']
    lat = position['lat']
    lng = position['lng']

    user = userscol.find_one({'_id': ObjectId(user_id)}, {
        '_id': 0, 'password': 0, 'name': 0, 'surname': 0, 'email': 0})

    print(title, description, address, price, date, date2,
          starting_time, expected_duration, position, lat, lng)

    othereventscol.insert({'img': 'assets/event.jpg', 'title': title, 'description': description, 'address': address,
                           'price': price, 'date': date, 'starting_time': starting_time,
                           'expected_duration': expected_duration, 'coordinates': {'lat': lat, 'lng': lng}, 'created_by': user['username']})
    return jsonify(message="event added")


@app.route('/signup', methods=['POST', 'GET'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def signup():
    data = request.get_json()
    name = data['firstname']
    surname = data['lastname']
    username = data['username']
    email = data['email']
    password = data['password']

    if not name:
        response = jsonify(message='No name inserted')
        response.status_code = 401
        return response

    if not surname:
        response = jsonify(message='No surname inserted')
        response.status_code = 401
        return response

    if not username:
        response = jsonify(message='No username inserted')
        response.status_code = 401
        return response

    if not email:
        response = jsonify(message='No email inserted')
        response.status_code = 401
        return response

    if not password:
        response = jsonify(message='No password inserted')
        response.status_code = 401
        return response

    if userscol.find_one({'email': email}):
        response = jsonify(message='Email already in use')
        response.status_code = 401
        return response

    if userscol.find_one({'username': username}):
        response = jsonify(message='username already in use')
        response.status_code = 401
        return response

    password = generate_password_hash(password)
    user_id = userscol.insert({'name': name, 'surname': surname, 'username': username,
                               'email': email, 'password': password, 'registered_at': datetime.now()})
    token = create_token(str(user_id))
    print(token)
    return jsonify(token=token)


if __name__ == '__main__':
    app.run(
        host=config.HOST,
        port=config.PORT,
        debug=config.DEBUG,
        threaded=config.THREADED
    )
